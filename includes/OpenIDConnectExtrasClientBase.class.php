<?php
/**
 * @file
 * Base class for OpenID Connect clients.
 * //@see https://github.com/jumbojett/OpenID-Connect-PHP/blob/master/OpenIDConnectClient.php5 authenticate()
 */
abstract class OpenIDConnectExtrasClientBase extends OpenIDConnectClientBase {
  
	protected $accessToken;
	
	protected $refreshToken;
  /**
   * @return string
   */
  public function getAccessToken() {
  	return $this->accessToken;
  }
  /**
   * @return string
   */
  public function getRefreshToken() {
    return $this->refreshToken;
  }
	/**
   * Get Issuer info
   * @see https://openid.net/specs/openid-connect-discovery-1_0.html#ProviderConfig
   */
  public function discoverRequest($issuer_url = NULL) {

		if ($issuer_url == NULL) return FALSE;
    
    $well_known_config_url = rtrim($issuer_url, "/") . "/.well-known/openid-configuration";
    $request_url = url($well_known_config_url, array('external' => TRUE));
    // get data from idp openid configuration
    $response = drupal_http_request($request_url); 
    if (!isset($response->error) && $response->code == 200) {
      $response_data = drupal_json_decode($response->data);
      return $response_data;    
    }
    else {
      openid_connect_log_request_error(__FUNCTION__, $this->name, $response);
      return FALSE;
    }
  }
  /**
   * Overrides OpenIDConnectClientBase::getEndpoints().
   * get endpoint data from idp automatically by attempting client discovery
   */
  public function getEndpoints() {
    
    $endpoints = array();
    if ($this->getSetting('dynareg')['enabled'] == 1) {
      $discovery_metadata = $this->discoverRequest($this->getSetting('dynareg')['issuer_url']);
      foreach ($discovery_metadata as $key => $value) {
        // all ocurrences ending with _endpoint
        if ( strpos($key, '_endpoint', strlen($key) - 9 ) ) {
          list($new_key, ) = explode('_endpoint',  $key); // remove '_endpoint' from string
          $endpoints[$new_key] = $value; 
        }
      }
    }

   return $endpoints;
  }
	/**
	 * we need to persist the refresh token information somehow in order to validate
	 * expiracy of the id_token, re use the refresh_token value at any time with endSession, etc..
	 * still need to decide what is the best approach to store values, session var / system variables ?
	 * using system variables
	 * @see openid_connect_admin_form_submit
	 * @see openid_connect_extras_user_logout
	 * @param $retrievedTokens array result from retrieveTokens
	 */
	private function processTokens($retrievedTokens) {
		
		$settings  = array(
			'client_id' => $this->getSetting('client_id'),
			'client_secret' => $this->getSetting('client_secret'),
			// dynareg keys
			'dynareg' => $this->getSetting('dynareg'),
		);
		// add refresh_Token and expire data to array
		$settings += array(
		  'id_token' => $retrievedTokens['id_token'],
			'refresh_token' => $retrievedTokens['refresh_token'],
			'access_token' => $retrievedTokens['access_token'],
			'expire' => $retrievedTokens['expire'],
		);
	  // set new client data including dynareg in openid_connect system variable
		variable_set('openid_connect_client_' . $this->name, $settings);
	}
  /**
   * Implements OpenIDConnectClientInterface::retrieveIDToken().
	 * include refresh token hook
   */
  public function retrieveTokens($authorization_code) {
    
    // Exchange `code` for access token and ID token.
    $redirect_uri = OPENID_CONNECT_REDIRECT_PATH_BASE . '/' . $this->name;
    $post_data = array(
      'code' => $authorization_code,
      'client_id' => $this->getSetting('client_id'),
      'client_secret' => $this->getSetting('client_secret'),
      'redirect_uri' => url($redirect_uri, array('absolute' => TRUE)),
      'grant_type' => 'authorization_code',
    );		
    $request_options = array(
      'method' => 'POST',
      'data' => drupal_http_build_query($post_data),
      'timeout' => 15,
      'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
    );
    $endpoints = $this->getEndpoints();
    $response = drupal_http_request($endpoints['token'], $request_options);

    if (!isset($response->error) && $response->code == 200) {
      $response_data = drupal_json_decode($response->data);
			//
			$this->accessToken = $response_data['access_token'];
			$this->refreshToken = $response_data['refresh_token'];
			// add values to our client info array for later use in end_session/refresh_token methods
			$retrievedTokens = array(
			  'id_token' => $response_data['id_token'],
        'access_token' => $response_data['access_token'],
        'refresh_token' => $response_data['refresh_token'],
        'expire' => REQUEST_TIME + $response_data['expires_in'],
      );
      // process tokens for later use
      $this->processTokens($retrievedTokens);

      return $retrievedTokens;
    }
    else {
      openid_connect_log_request_error(__FUNCTION__, $this->name, $response);
      return FALSE;
    }
  }
  /**
   * Refresh Token
   * @see http://openid.net/specs/openid-connect-standard-1_0-21.html#RefreshingAccessToken
   */
  public function refreshAccessTokens($refresh_token) {

    $post_data = array(
      'client_id' => $this->getSetting('client_id'),
      'client_secret' => $this->getSetting('client_secret'),
      'grant_type' => 'refresh_token',
      'refresh_token' => $refresh_token,
    );
    $request_options = array(
      'method' => 'POST',
      'data' => drupal_http_build_query($post_data),
      'timeout' => 15,
      'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
    );
		$endpoints = $this->getEndpoints();
    $request_url = url($endpoints['token'], array('external' => TRUE));
    $response = drupal_http_request($request_url, $request_options);
    // watchdog('refreshtoken', print_r($response,1));
    if (!isset($response->error) && $response->code == 200) {
      $response_data = drupal_json_decode($response->data);
      
      // refresh values in array
      $retrievedTokens = array(
        'id_token' => $response_data['id_token'],
        'access_token' => $response_data['access_token'],
        'refresh_token' => $response_data['refresh_token'],
        'expire' => REQUEST_TIME + $response_data['expires_in'],
      );
      // process tokens for later use
      $this->processTokens($retrievedTokens);

      return $retrievedTokens;
    }
    else {
      openid_connect_log_request_error(__FUNCTION__, $this->name, $response);
      return FALSE;
    }    
  }
  /**
   * End User Session in IdP
	 * @see http://openid.net/specs/openid-connect-session-1_0-15.html
   * @see http://openid.net/specs/openid-connect-session-1_0-15.html#RPLogout
	 * @param id_token a NON expired id_token (RECOMMENDED)
	 * @param $post_logout_redirect_url logout url registered on the IdP (OPTIONAL) if present, the standard stands we need to do perform a redirect to the registered post logout redirect uri
   */
  public function endSession($id_token, $post_logout_redirect_url = '') {
    $endpoints = $this->getEndpoints();
		//
		if($post_logout_redirect_url != '') {
			$url_options = array('query' => array(
					'id_token_hint' => $id_token,
					'post_logout_redirect_uri' => htmlentities($post_logout_redirect_url),
				)
			);
	  	drupal_goto($endpoints['end_session'], $url_options);
		}
		//
		$post_data = array(
      'id_token_hint' => $id_token,
    );
    $request_options = array(
      'method' => 'POST',
      'data' => drupal_http_build_query($post_data),
      'timeout' => 15,
      'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
    );	
		$request_url = url($endpoints['end_session'], array('external' => TRUE));
		$response = drupal_http_request($request_url, $request_options);

    if (!isset($response->error) && $response->code == 200) {
      $response_data = drupal_json_decode($response->data);
		} else {
			openid_connect_log_request_error(__FUNCTION__, $this->name, $response);
      return FALSE;
		}
  }
}
