This module provides new methods not yet implemented in OpenID Connect
  discoverRequest
  refreshAccessTokens
  requestAccessTokens
  endSession
  
Overrrides openid_connect retrieveTokens method in order to inject a method that will handle the refresh_token and expire info, required by end_session, etc. temporarly
See the comments on the functions/methods for more information

USAGE:
- Enable OpenID Connect Extras as usual.
- Create your client as instructed by the OpenID Connect module.
- Instead of using the extend class mentioned by the OIDC documentation we will use the class provided by this module, so the client definition will look similar to:

  /**
   * Implements OpenID Connect Client plugin for Gluu
   */
  class OpenIDConnectClientGluu  extends OpenIDConnectExtrasClientBase {
  ...
  ...
  ...
  }
