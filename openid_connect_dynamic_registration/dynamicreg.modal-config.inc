<?php
/**
 * @file
 * Ctools Modal Window
 * Register Configuration Form
 */

/**
 * open dynamic registration form
 */
function openid_connect_dynamic_registration_register_ctools_modal($client = null, $js = null) {
  
  if (!$js)
    return "Javascript required";

  ctools_include('modal');
  ctools_include('ajax');

  $form_state = array(
    'title' => t('Configure Client'), 
    'ajax' => $js,
  );
  
	// load dynamic registration data
	$dr = openid_connect_get_client($client);
	$obj = $dr->getSetting('dynareg');
	$form_state['build_info']['args']['dynareg'] = $obj;
	$form_state['build_info']['args']['dynareg']['client'] = $client;
  $commands = array();
  $commands = ctools_modal_form_wrapper('openid_connect_dynamic_registration_client_configuration_form', $form_state);
  if ($form_state['executed'] == TRUE && !isset($form_state['close'])) {
    $commands[] = ctools_ajax_command_reload();   
  }
  print ajax_render($commands);
  drupal_exit();
}

/**
 * OpenID Connect Dynamic Registration Form (Discovery)
 * 
 * Build a settings form based on the values defined on the oidc standard
 * attach submitted values to oidc module variables
 * default field values are checked against idp /.well-known/openid-configuration provided; else default metadata is returned defined by the standard
 * 
 * @see http://openid.net/specs/openid-connect-registration-1_0.html#ClientMetadata
 * @see http://hdknr.github.io/docs/identity/reg.html
 */
function openid_connect_dynamic_registration_client_configuration_form($form, &$form_state) {
  // get client name  
  $client_name = $form_state['build_info']['args']['dynareg']['client'];
  // get issuer url
  $issuer_url = $form_state['build_info']['args']['dynareg']['issuer_url'];
  // get dynareg info 
  $dr = openid_connect_get_client($client_name);	
  // get metadata from idp
  $client_name_meta_data = $dr->discoverRequest($issuer_url);
  // get default metadata
  $default_meta_data = openid_connect_dynamic_registration_get_oidc_discover_default_metadata();
  // get dynareg settings form data
  $obj = $dr->getSetting('dynareg');
	$dr_info = $obj['dynareg_config_form'];
	//
  $form['#prefix'] = '<div id = "openid-connect-client-settings-form">';
  $form['#suffix'] = '</div>';
  
	$index = 'dynareg_' . $client_name;
  $form[$index] = array(
    '#title' => t('Client Settings'),
    '#type' => 'fieldset',
    '#tree' => TRUE,
  );
  // show issuer url
  $form[$index]['idpname'] = array('#type' => 'markup', '#markup' => '<b>' . t('Issuer:') . '</b>' . '&nbsp;' . $issuer_url);
  //Registration Endpoint
  $description = ($client_name_meta_data['registration_endpoint']) ? $client_name_meta_data['registration_endpoint'] : $default_meta_data['registration_endpoint'];
  $form[$index]['registration_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Registration Endpoint'),
    '#default_value' => isset($client_name_meta_data['registration_endpoint']) ? $client_name_meta_data['registration_endpoint'] : '',
    '#maxlength' => 2048,
    '#size' => 80,
    '#required' => TRUE,
    '#disabled' => isset($client_name_meta_data['registration_endpoint']), 
    '#description' => $description,
  );
  // Redirect URLs
  $description = '';
  $form[$index]['redirect_uris'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirect URIs'),
    '#required' => TRUE,
    '#default_value' => url(OPENID_CONNECT_REDIRECT_PATH_BASE . '/' . $client_name, array('absolute' => TRUE, 'https' => TRUE)),
    '#maxlength' => 2048,
    '#size' => 80,    
    '#disabled' => TRUE,
    '#description' => t('This value is already provided by the OpenID Connect module.'),
  );
  // Response Types
  $options = ($client_name_meta_data['response_types_supported']) ? array_combine($client_name_meta_data['response_types_supported'], $client_name_meta_data['response_types_supported']) : $default_meta_data['response_types'];
  $form[$index]['response_types'] = array(
    '#type' => 'select',
    '#title' => t('Response Types'),
    '#default_value' => $dr_info['response_types'],
    '#options' => $options,
    '#multiple' => TRUE,
    '#size' => count($options),
    '#description' => t('Description for Response Types.'),
  );
  // Grant Types
  $options = ($client_name_meta_data['grant_types_supported']) ? array_combine($client_name_meta_data['grant_types_supported'], $client_name_meta_data['grant_types_supported']) : $default_meta_data['grant_types'];
  $form[$index]['grant_types'] = array(
    '#type' => 'select',
    '#title' => t('Grant Types'),
    '#default_value' => $dr_info['grant_types'],
    '#options' => $options,
    '#multiple' => TRUE,
    '#size' => count($options),
    '#description' => t('Description for Grant Types.'),  
  );
  // Application Type
  $options = array('web' => 'web', 'native' => 'native');
  $form[$index]['application_type'] = array(
    '#type' => 'select',
    '#title' => t('Application Type'),
    //'#empty_value' => 0,
    '#default_value' => $dr_info['application_type'],
    '#empty_option' => t('- Select -'),
    '#options' => $options,
    '#description' => t('Description for Application Type.'),
  );
  // Contacts
  $site_mail = variable_get('site_mail', ini_get('sendmail_from'));
  $description = ($site_mail != '') ? $site_mail : 'sitemail@local.com, sitemail2@local.com';
  $form[$index]['contacts'] = array(
    '#type' => 'textfield',
    '#title' => t('Contacts'),
    '#default_value' => $dr_info['contacts'],
    '#maxlength' => 2048,
    '#size' => 80,      
    '#description' => t('Enter comma separated values. Example:') . '&nbsp;' . $description,
  );
  // Client Name
  $description = variable_get('site_name', 'Test Client');
  $form[$index]['client_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Client Name'),
    '#default_value' => $dr_info['client_name'],
    '#maxlength' => 2048,
    '#size' => 80,  
    '#description' => t('Enter a Client Name. Example:') . '&nbsp;' .  $description,
  );
  // Logo URI
  $logo = theme_get_setting('logo');
  $description = (isset($logo)) ? url($logo, array('absolute' => TRUE)) : 'http://mypathtomylogo.png';
  $form[$index]['logo_uri'] = array(
    '#type' => 'textfield',
    '#title' => t('Logo URI'),
    '#default_value' => $dr_info['logo_uri'],
    '#maxlength' => 2048,
    '#size' => 80,  
    '#description' => t('Example:') . '&nbsp;' . $description,
  );
  //Client URI
  $description = (!empty($base_url))?url($base_url, array('absolute' => TRUE, 'https' =>TRUE)):url('', array('absolute' => TRUE, 'https' =>TRUE));
  $form[$index]['client_uri'] = array(
    '#type' => 'textfield',
    '#title' => t('Client URI'),
    '#default_value' => $dr_info['client_uri'],
    '#maxlength' => 2048,
    '#size' => 80,  
    '#description' => t('Enter Client URL. Example:') . '&nbsp;' . $description,
  );
  // Policy URI
  $description = ($client_name_meta_data['op_policy_uri']) ? $client_name_meta_data['op_policy_uri'] : $default_meta_data['policy_uri'];
  $form[$index]['policy_uri'] = array(
    '#type' => 'textfield',
    '#title' => t('Policy URI'),
    '#default_value' => $dr_info['policy_uri'],
    '#maxlength' => 2048,
    '#size' => 80,  
    '#description' => t('Example:') . '&nbsp;' . $description,
  );
  // Terms of Service URI
  $description = ($client_name_meta_data['op_tos_uri']) ? $client_name_meta_data['op_tos_uri']:$default_meta_data['tos_uri'];
  $form[$index]['tos_uri'] = array(
    '#type' => 'textfield',
    '#title' => t('Terms of Service URI'),
    '#default_value' => $dr_info['tos_uri'],
    '#maxlength' => 2048,
    '#size' => 80,  
    '#description' => t('Example:') . '&nbsp;' . $description,
  );
  // jwks_uri
  $description = ($client_name_meta_data['jwks_uri']) ? $client_name_meta_data['jwks_uri']:$default_meta_data['jwks_uri'];
  $form[$index]['jwks_uri'] = array(
    '#type' => 'textfield',
    '#title' => t('JWKS URI'),
    '#default_value' => $dr_info['jwks_uri'],
    '#maxlength' => 2048,
    '#size' => 80,  
    '#description' => t('Example:') . '&nbsp;' . $description,   
  );
  // jwks
  $description = 'http://description';
  $form[$index]['jwks'] = array(
    '#type' => 'textfield',
    '#title' => t('JWKS'),
    '#default_value' => $dr_info['jwks'],
    '#maxlength' => 2048,
    '#size' => 80,      
    '#description' => t('Example:') . '&nbsp;' . $description,   
  );
  // sector_identifier_uri
  $description = 'http://sector.url';
  $form[$index]['sector_identifier_uri'] = array(
    '#type' => 'textfield',
    '#title' => t('Sector Identifier URI'),
    '#default_value' => $dr_info['sector_identifier_uri'],
    '#maxlength' => 2048,
    '#size' => 80,      
    '#description' => t('Example:') . '&nbsp;' . $description,   
  );
  // subject_type
  $options = ($client_name_meta_data['subject_types_supported']) ? array_combine($client_name_meta_data['subject_types_supported'], $client_name_meta_data['subject_types_supported']) : $default_meta_data['subject_type'];
  $form[$index]['subject_type'] = array(
    '#type' => 'select',
    '#title' => t('Subject Type'),
    '#empty_option' => t('- Select -'),
    '#default_value' => $dr_info['subject_type'],
    '#options' => $options,
    '#description' => t('Description for Subject Type.'),
  );
  // id_token_signed_response_alg
  $options = ($client_name_meta_data['id_token_signing_alg_values_supported']) ? array_combine($client_name_meta_data['id_token_signing_alg_values_supported'], $client_name_meta_data['id_token_signing_alg_values_supported']) : $default_meta_data['id_token_signed_response_alg'];
  $form[$index]['id_token_signed_response_alg'] = array(
    '#type' => 'select',
    '#title' => t('Id Token Signed Response Algorithm'),
    '#empty_option' => t('- Select -'),
    '#default_value' => $dr_info['id_token_signed_response_alg'],
    '#options' => array_merge(array('none' => 'NONE') , $options),
    '#description' => t('Description for id_token_signed_response_alg.'),  
  );
  // id_token_encrypted_response_alg
  $options = ($client_name_meta_data['id_token_encryption_alg_values_supported']) ? array_combine($client_name_meta_data['id_token_encryption_alg_values_supported'], $client_name_meta_data['id_token_encryption_alg_values_supported']) : $default_meta_data['id_token_encrypted_response_alg'];
  $form[$index]['id_token_encrypted_response_alg'] = array(
    '#type' => 'select',
    '#title' => t('Id Token Encrypted Response Algorithm'),
    '#empty_option' => t('- Select -'),
    '#default_value' => $dr_info['id_token_encrypted_response_alg'],
    '#options' => $options,
    '#description' => t('Description for id_token_encrypted_response_alg.'),  
  );
  // id_token_encrypted_response_enc
  $options = ($client_name_meta_data['id_token_encryption_enc_values_supported']) ? array_combine($client_name_meta_data['id_token_encryption_enc_values_supported'], $client_name_meta_data['id_token_encryption_enc_values_supported']) : $default_meta_data['id_token_encrypted_response_enc'];
  $form[$index]['id_token_encrypted_response_enc'] = array(
    '#type' => 'select',
    '#title' => t('Id Token Encrypted Response enc Algorithm'),
    '#empty_option' => t('- Select -'),
    '#default_value' => $dr_info['id_token_encrypted_response_enc'],
    '#options' => $options,
    '#description' => t('Description for id_token_encrypted_response_enc.'),  
  );
  // userinfo_signed_response_alg
  $options = ($client_name_meta_data['userinfo_signing_alg_values_supported']) ? array_combine($client_name_meta_data['userinfo_signing_alg_values_supported'], $client_name_meta_data['userinfo_signing_alg_values_supported']) : $default_meta_data['userinfo_signed_response_alg'];
  $form[$index]['userinfo_signed_response_alg'] = array(
    '#type' => 'select',
    '#title' => t('Userinfo Signed Response Algorithm'),
    '#empty_option' => t('- Select -'),
    '#default_value' => $dr_info['userinfo_signed_response_alg'],
    '#options' => array_merge(array('NONE' => 'NONE'), $options),
    '#description' => t('Description for userinfo_signed_response_alg.'),    
  );
  // userinfo_encrypted_response_alg
  $options = ($client_name_meta_data['userinfo_encryption_alg_values_supported']) ? array_combine($client_name_meta_data['userinfo_encryption_alg_values_supported'], $client_name_meta_data['userinfo_encryption_alg_values_supported']) : $default_meta_data['userinfo_encrypted_response_alg'];
  $form[$index]['userinfo_encrypted_response_alg'] = array(
    '#type' => 'select',
    '#title' => t('Userinfo Encrypted Response Algorithm'),
    '#empty_option' => t('- Select -'),
    '#default_value' => $dr_info['userinfo_encrypted_response_alg'],
    '#options' => $options,
    '#description' => t('Description for userinfo_encrypted_response_alg.'),    
  );
  // userinfo_encrypted_response_enc
  $options = ($client_name_meta_data['userinfo_encryption_enc_values_supported']) ? array_combine($client_name_meta_data['userinfo_encryption_enc_values_supported'], $client_name_meta_data['userinfo_encryption_enc_values_supported']) : $default_meta_data['userinfo_encrypted_response_enc'];
  $form[$index]['userinfo_encrypted_response_enc'] = array(
    '#type' => 'select',
    '#title' => t('Userinfo Encrypted Response enc Algorithm'),
    '#empty_option' => t('- Select -'),
    '#default_value' => $dr_info['userinfo_encrypted_response_enc'],
    '#options' => $options,
    '#description' => t('Description for userinfo_encrypted_response_enc.'),    
  );
  // request_object_signing_alg
  $options = ($client_name_meta_data['request_object_signing_alg_values_supported']) ? array_combine($client_name_meta_data['request_object_signing_alg_values_supported'], $client_name_meta_data['request_object_signing_alg_values_supported']) : $default_meta_data['request_object_signing_alg'];
  $form[$index]['request_object_signing_alg'] = array(
    '#type' => 'select',
    '#title' => t('Request Object  Signing Algorithm'),
    '#empty_option' => t('- Select -'),
    '#default_value' => $dr_info['request_object_signing_alg'],
    '#options' => array_merge(array('none' => 'NONE'), $options),
    '#description' => t('Description for request_object_signing_alg.'),     
  );
  // request_object_encryption_alg
  $options = ($client_name_meta_data['request_object_encryption_alg_values_supported']) ? array_combine($client_name_meta_data['request_object_encryption_alg_values_supported'], $client_name_meta_data['request_object_encryption_alg_values_supported']) : $default_meta_data['request_object_encryption_alg'];
  $form[$index]['request_object_encryption_alg'] = array(
    '#type' => 'select',
    '#title' => t('Request Object Encryption Algorithm'),
    '#empty_option' => t('- Select -'),
    '#default_value' => $dr_info['request_object_encryption_alg'],
    '#options' => $options,
    '#description' => t('Description for request_object_encryption_alg.'),   
  );
  // request_object_encryption_enc
  $options = ($client_name_meta_data['request_object_encryption_enc_values_supported']) ? array_combine($client_name_meta_data['request_object_encryption_enc_values_supported'], $client_name_meta_data['request_object_encryption_enc_values_supported']) : $default_meta_data['request_object_encryption_enc'];
  $form[$index]['request_object_encryption_enc'] = array(
    '#type' => 'select',
    '#title' => t('Request Object Encryption enc Alogrithm'),
    '#empty_option' => t('- Select -'),
    '#default_value' => $dr_info['request_object_encryption_enc'],
    '#options' => $options,
    '#description' => t('Description for request_object_encryption_enc.'),     
  );
  // token_endpoint_auth_method
  $options = ($client_name_meta_data['token_endpoint_auth_methods_supported']) ? array_combine($client_name_meta_data['token_endpoint_auth_methods_supported'], $client_name_meta_data['token_endpoint_auth_methods_supported']) : $default_meta_data['token_endpoint_auth_method'];
  $form[$index]['token_endpoint_auth_method'] = array(
    '#type' => 'select',
    '#title' => t('Token Endpoint Auth Method'),
    '#empty_option' => t('- Select -'),
    '#default_value' => $dr_info['token_endpoint_auth_method'],
    '#options' => $options,
    '#description' => t('Token Endpoint Auth Method.'),
  );
  // token_endpoint_auth_signing_alg
  $options = ($client_name_meta_data['token_endpoint_auth_signing_alg_values_supported']) ? array_combine($client_name_meta_data['token_endpoint_auth_signing_alg_values_supported'], $client_name_meta_data['token_endpoint_auth_signing_alg_values_supported']) : $default_meta_data['token_endpoint_auth_signing_alg'];
  $form[$index]['token_endpoint_auth_signing_alg'] = array(
    '#type' => 'select',
    '#title' => t('Token Endpoint Sign Alg'),
    '#empty_option' => t('- Select -'),
    '#default_value' => $dr_info['token_endpoint_auth_signing_alg'],
    '#options' => $options,
    '#description' => t('Token Endpoint Sign Alg.'),
  );
  // Default Max Age
  $description = '';
  $form[$index]['default_max_age'] = array(
    '#type' => 'textfield',
    '#title' => t('Default Max Age'),
    '#default_value' => $dr_info['default_max_age'],
    '#maxlength' => 2048,
    '#size' => 80,      
    '#description' => t('Enter value in seconds.'),
  );
  // Require Auth Time
  $description = '';
  $form[$index]['require_auth_time'] = array(
    '#type' => 'radios',
    '#title' => t('Require Auth Time'),
    '#options' => array('false' => 'no', 'true' => 'yes'),
    '#default_value' => $dr_info['require_auth_time'],
    '#description' => t('Description for Require auth time'),
  ); 
  // Default ACR Values
  $description = '';
  $default_acr_values = ($client_name_meta_data['acr_values_supported']) ? $client_name_meta_data['acr_values_supported'] : $default_meta_data['acr_values'];
  $form[$index]['default_acr_values'] = array(
    '#type' => 'textfield',
    '#title' => t('Default ACR values'),
    '#default_value' => $dr_info['default_acr_values'],
    '#maxlength' => 2048,
    '#size' => 80,      
    '#description' => t('Enter comma separated values.') . '&nbsp;' . $description,
  );
  // Initiate login URI
  $description = '';
  $form[$index]['initiate_login_uri'] = array(
    '#type' => 'textfield',
    '#title' => t('Initiate Login URI'),
    '#default_value' => $dr_info['initiate_login_uri'],
    '#maxlength' => 2048,
    '#size' => 80,      
    '#description' => t('Description for Initiate Login URI') . '&nbsp;' . $description,
  );
  //Post Logout Redirect URIs
  $description = (!empty($base_url)) ? url($base_url, array('absolute' => FALSE, 'https'=>TRUE)) : url('', array('absolute' => TRUE, 'https'=>TRUE));
  $form[$index]['post_logout_redirect_uris'] = array(
    '#type' => 'textfield',
    '#title' => t('Post Logout Redirect URIs'),
    '#default_value' => $dr_info['post_logout_redirect_uris'],
    '#maxlength' => 2048,
    '#size'=> 80,      
    '#description' => t('Enter comma separated values. Example:') . '&nbsp;' . $description,
  );
  // Scopes
  $description = (isset($client_name_meta_data['scopes_supported'])) ? implode(',', $client_name_meta_data['scopes_supported']) : 'openid, email, address';
  $form[$index]['scopes'] = array(
    '#type' => 'textfield',
    '#title' => t('Scopes'),
    '#default_value' => $dr_info['scopes'],
    '#maxlength' => 2048,
    '#size' => 80,      
    '#description' => t('Enter comma separated values. Example:') . '&nbsp;' . $description,
  );
  // Request URIs
  $description = 'https://www.myurl1.com, https://www.myurl2.com';
  $form[$index]['request_uris']  = array(
    '#type' => 'textfield',
    '#title' => t('Request URIs'),
    '#default_value' => $dr_info['request_uris'],
    '#maxlength' => 2048,
    '#size' => 80,  
    '#description' => t('Enter comma separated values. Example:') . '&nbsp;' . $description,
  );
	$form['submit']  = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  // register custom handlers
  $form['#validate'] = array('openid_connect_dynamic_registration_client_configuration_form_validate');
  $form['#submit'] = array('openid_connect_dynamic_registration_client_configuration_form_submit');

  return $form;
}
/**
 * hook_validate
 * most validation is performed on the idp we must catch errors
 */
function openid_connect_dynamic_registration_client_configuration_form_validate($form, &$form_state) {
	
  // get client name  
  $client_name = $form_state['build_info']['args']['dynareg']['client'];
  // get issuer url
  $issuer_url = $form_state['build_info']['args']['dynareg']['issuer_url'];
  // filter dynareg fields
  foreach ($form_state['values'] as $item => $value) {
    if (strpos($item, 'dynareg_' . $client_name) !== FALSE) {
      $settings[$item] = $value; 
    }
  }
	// dynamic reg obj
  $dr = openid_connect_get_client($client_name);//openid_connect_dynamic_registration_get_dynareg($issuer_url, $client_name);
  // get idp info
  $dynamic_client_data = openid_connect_dynamic_registration_register_request($settings['dynareg_' . $client_name]);//$dr->sendDynamicRegistrationRequest($settings['dynareg_' . $client_name]);
  // show error if something fails
  if (isset($dynamic_client_data->error) OR isset($dynamic_client_data->error_code)) {
	  $msg .= 'Error: ' . $dynamic_client_data->error . '</br>';
	  $msg .= 'Error code: ' . $dynamic_client_data->code . '</br>';
	  $msg .= 'Error desc: ' . $dynamic_client_data->description;
		drupal_set_message($msg, 'error');
	  form_set_error('', t('Could not register the client, please check the configuration options and try again.'));
	}
	// no errors - pass dynare object to submit
	$form_state['values']['dynareg_' . $client_name . '_reginfo'] = $dynamic_client_data;
}
/**
 * hook_form_submit()
 * include dyanamic registration information in the openid_connect system variable
 * @see openid_connect_admin_form_submit
 */
function openid_connect_dynamic_registration_client_configuration_form_submit($form, &$form_state) {
	/* 
  $client_name = $form_state['build_info']['args']['dynareg']['client'];
	$form_state['values'] = array(
		'openid_connect_clients_enabled' => array_combine($client_name),
		'openid_connect_client_' . $client_name => array(
			'dynareg' => array(
				'dynareg_' . $client_name . '_reginfo' => $form_state['values']['dynareg_' . $client_name . '_reginfo'],
				'dynareg_' . $client_name . '_form' =>	$form_state['values']['dynareg_' . $client_name],
			),
		),
	);
	drupal_form_submit('openid_connect_admin_form', $form_state);
	*/
	// get client name  
  $client_name = $form_state['build_info']['args']['dynareg']['client'];
  // get issuer url
  $issuer_url = $form_state['build_info']['args']['dynareg']['issuer_url'];
	// get valid data
  $dynamic_client_data = $form_state['values']['dynareg_' . $client_name . '_reginfo'];
	// set new client_id and secret for client in openid_connect system variable and keep dynareg data persistent
	$dynamic_client_form = $form_state['values']['dynareg_' . $client_name];
	//
	$client  = array(
		//openid_connect keys
		'client_id' => $dynamic_client_data['client_id'],
		'client_secret' => $dynamic_client_data['client_secret'],
		//dynareg keys
		'dynareg' => array(
			'enabled' => 1, 
			'issuer_url' => $issuer_url, 
			'dynareg_reginfo' => $dynamic_client_data, 
			'dynareg_config_form' => $dynamic_client_form
		),
	);
  // set new client data including dynareg in openid_connect system variable
	variable_set('openid_connect_client_' . $client_name, $client);
	//
  drupal_set_message(t('Client successfully registered.'));
}