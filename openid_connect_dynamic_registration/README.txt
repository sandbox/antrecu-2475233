Attempts register a client on a given IdP (issuerl url)
appends/stores registration info on openid_connect_$client_name system varialble

USAGE:
- Enable module as usual.
- Visit the OpenID Connect admin UI. admin/config/services/openid-connect
- Check the box for enable Dynamic Registration for your desired client.
- Save configuration.
- Click on configure client. Fill in the values on the register form. 

If the registratation is succeful the client_id/secret combination will be set automatically, else
the errors related to the request will be displayed on the modal window so the user can try again.


